﻿namespace TP1_Florian_Metz
{
    public static class FizzBuzz
    {
        public static string Generer(int n)
        {
            if (n is < 15 or > 150)
                throw new ArgumentOutOfRangeException("n", "should be between 15 and 150");

            var generated = "";
            for (var i = 1; i <= n; i++)
            {
                generated += Transform(i);
            }
            return generated;
        }

        public static string Transform(int number)
        {
            if (number % 3 == 0 && number % 5 == 0)
                return "FizzBuzz";
            if (number % 3 == 0)
                return "Fizz";
            if (number % 5 == 0)
                return "Buzz";

            return $"{number}";
        }
    }
}