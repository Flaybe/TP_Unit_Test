﻿using Microsoft.VisualBasic;

namespace TP1_Florian_Metz
{
    public static class TennisGame
    {
        public static string DisplayScore(int won1, int won2)
        {
            var errorMessage = "won1 and won2 must be greater than 0";
            if (won1 < 0)
                throw new ArgumentOutOfRangeException("won1", errorMessage);
            if (won2 < 0)
                throw new ArgumentOutOfRangeException("won2", errorMessage);

            var scores = new[] { "0", "15", "30", "40", "50" };
            var score1 = CalculEasyScore(won1);
            var score2 = CalculEasyScore(won2);
            if (won1 == won2 + 2 && (won1 > 3 && won2 > 3))
            {
                score1 = "50";
                score2 = "40";
            }
            if (won2 == won1 + 2 && (won1 > 3 && won2 > 3))
            {
                score1 = "40";
                score2 = "50";
            }
            if (won1 == won2 + 1 && (won1 > 3 && won2 > 3))
            {
                score1 = "adv";
                score2 = "40";
            }
            if (won2 == won1 + 1 && (won1 > 3 && won2 > 3))
            {
                score1 = "40";
                score2 = "adv";
            }

            return $"{score1}-{score2}";
        }

        public static string CalculEasyScore(int won)
        {
            switch (won)
            {
                case 0:
                    return "0";
                    break;
                case 1:
                    return "15";
                    break;
                case 2:
                    return "30";
                    break;
                case 3:
                    return "40";
                    break;
                default:
                    return "";
                    break;
            }
        }
    }
}
