﻿using TP1_Florian_Metz;
using Xunit.Abstractions;

namespace UnitTests
{
    public class TennisGameTest
    {
        private static ITestOutputHelper _output;

        public TennisGameTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData(0,0)]
        [InlineData(7,5)]
        [InlineData(-4,-5)]
        [InlineData(-12,12)]
        [InlineData(7,-5)]
        public void ParameterTest(int won1, int won2)
        {
            if(won1 < 0 || won2 < 0)
                Assert.Throws<ArgumentOutOfRangeException>(() => TennisGame.DisplayScore(won1, won2));
            else
                Assert.NotEmpty(TennisGame.DisplayScore(won1, won2));
        }

        [Fact]
        public void DisplayTest()
        {
            _output.WriteLine(TennisGame.DisplayScore(0, 0));
            _output.WriteLine(TennisGame.DisplayScore(1, 1));
            _output.WriteLine(TennisGame.DisplayScore(2, 3));
            _output.WriteLine(TennisGame.DisplayScore(4, 5));
            _output.WriteLine(TennisGame.DisplayScore(5,4));
            _output.WriteLine(TennisGame.DisplayScore(7, 5));
            _output.WriteLine(TennisGame.DisplayScore(5,7));

            Assert.Equal("0-0", TennisGame.DisplayScore(0, 0));
            Assert.Equal("15-15", TennisGame.DisplayScore(1,1));
            Assert.Equal("30-40", TennisGame.DisplayScore(2,3));
            Assert.Equal("40-adv", TennisGame.DisplayScore(4,5));
            Assert.Equal("adv-40", TennisGame.DisplayScore(5,4));
            Assert.Equal("50-40", TennisGame.DisplayScore(7,5));
            Assert.Equal("40-50", TennisGame.DisplayScore(5,7));
        }

        [Fact]
        public void CalculEasyScoreTest()
        {
            Assert.Equal("0", TennisGame.CalculEasyScore(0));
            Assert.Equal("15", TennisGame.CalculEasyScore(1));
            Assert.Equal("30", TennisGame.CalculEasyScore(2));
            Assert.Equal("40", TennisGame.CalculEasyScore(3));
            Assert.Equal("", TennisGame.CalculEasyScore(32));
        }
    }
}