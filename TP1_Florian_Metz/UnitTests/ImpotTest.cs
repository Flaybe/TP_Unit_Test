﻿using Impots;

namespace UnitTests
{
    public class ImpotTest
    {
        [Theory]
        [InlineData(0, 0)]
        [InlineData(10000, 0)]
        [InlineData(10778, 11)]
        [InlineData(27479, 30)]
        [InlineData(78571, 41)]
        [InlineData(168995, 45)]
        [InlineData(1689905, 45)]
        public void CalculTauxTest(int revenuAnnuel, int expected)
        {
            Assert.Equal(expected, Impot.CalculTaux(revenuAnnuel));
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(10000, 0)]
        [InlineData(10778, 1185.58)]
        [InlineData(27479, 8243.7)]
        [InlineData(78571, 32214.11)]
        [InlineData(168995, 76047.75)]
        [InlineData(1689905, 760457.25)]
        public void CalculValeurImpot(int revenuAnnuel, float expected)
        {
            Assert.Equal(expected, Impot.CalculValeurImpot(revenuAnnuel));

        }
    }
}
