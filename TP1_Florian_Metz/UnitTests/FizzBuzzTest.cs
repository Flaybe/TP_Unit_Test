using TP1_Florian_Metz;
using Xunit.Abstractions;

namespace UnitTests
{
    public class FizzBuzzTest
    {
        private static ITestOutputHelper _output;

        public FizzBuzzTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData(-500)]
        [InlineData(0)]
        [InlineData(10)]
        [InlineData(15)]
        [InlineData(75)]
        [InlineData(150)]
        [InlineData(200)]
        public void ParameterSizeTest(int n)
        {
            if (n is < 15 or > 150)
            {
                Assert.Throws<ArgumentOutOfRangeException>( () => FizzBuzz.Generer(n));
            }
            else
            {
                Assert.NotEmpty(FizzBuzz.Generer(n));
            }
        }

        [Theory]
        [InlineData(15)]
        [InlineData(75)]
        [InlineData(150)]
        public void GenererTest(int n)
        {
            var result = FizzBuzz.Generer(n);
            Assert.DoesNotContain(result, "5");
            Assert.DoesNotContain(result, "0");

            Assert.Contains("Fizz", result);
            Assert.Contains("Buzz", result);
            Assert.Contains("FizzBuzz", result);

            _output.WriteLine(result);
        }


        [Fact]
        public void TransformTest()
        {
            Assert.Equal("Fizz", FizzBuzz.Transform(3));
            Assert.Equal("Buzz", FizzBuzz.Transform(5));
            Assert.Equal("FizzBuzz", FizzBuzz.Transform(15));
            Assert.Equal("47", FizzBuzz.Transform(47));
        }
    }
}