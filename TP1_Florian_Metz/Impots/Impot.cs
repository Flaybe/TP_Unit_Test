﻿namespace Impots
{
    public static class Impot
    {
        public static float CalculValeurImpot(int revenuAnnuel)
        {
            var taux = CalculTaux(revenuAnnuel);
            var valeur = (float)taux / 100 * revenuAnnuel;
            return valeur;
        }

        public static int CalculTaux(int revenu)
        {
            switch (revenu)
            {
                case <= 10777 and >= 0:
                    return 0;
                case <= 27478 and >= 10778:
                    return 11;
                case <= 78570 and >= 27479:
                    return 30;
                case <= 168994 and >= 78571:
                    return 41;
                case >= 168995:
                    return 45;

            }

            return 0;
        }
    }
}
