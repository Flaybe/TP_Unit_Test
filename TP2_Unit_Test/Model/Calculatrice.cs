﻿namespace Model
{
    public static class Calculatrice
    {
        public static double CalculMensualite(int emprunt, int dureeAnnee, double taux)
        {
            var nbMoisAnnee = 12;
            var tauxMensuel = (taux/100) / nbMoisAnnee;
            
            var nbMensualites = dureeAnnee * nbMoisAnnee;
            
            var puissance = Math.Pow(1 + tauxMensuel, -nbMensualites);
            var mensualite = (emprunt * tauxMensuel) / (1 - puissance);
            
            return Math.Round(mensualite, 2);
        }

        public static double CalculTauxAssurance(bool estSportif, bool estFumeur, bool estCardiaques, bool estIngenieur,
            bool estPilote)
        {
            var variation = 0.3;
            if (estSportif)
                variation -= 0.05;
            
            if (estFumeur)
                variation += 0.15;

            if (estCardiaques)
                variation += 0.3;

            if (estIngenieur)
                variation -= 0.05;

            if (estPilote)
                variation += 0.15;

            return Math.Round(variation, 2);
        }

        public static double CalculTotalAPayer(int emprunt, double taux)
        {
            return emprunt + (emprunt * taux/100);
        }
    }
}