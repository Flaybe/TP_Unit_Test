using Model;

namespace TP2_Tests
{
    public class CalculatriceTest
    {
        [Theory]
        [InlineData(100000, 10, 2.47, 941.34)]
        [InlineData(150000, 15, 2.67, 1012.23)]
        [InlineData(250000, 25, 3, 1185.53)]
        [InlineData(250000, 25, 0.3, 865.08)]
        public void ShouldReturnGoodMensualite(int emprunt, int dureeAnnee, double taux, double expected)
        {
            var result = Calculatrice.CalculMensualite(emprunt, dureeAnnee, taux);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(false, false, false, false, false, 0.3)]
        [InlineData(true, false, false, false, false, 0.25)]
        [InlineData(false, true, false, false, false, 0.45)]
        [InlineData(false, false, true, false, false, 0.6)]
        [InlineData(false, false, false, true, false, 0.25)]
        [InlineData(false, false, false, false, true, 0.45)]
        [InlineData(true, true, true, true, true, 0.80)]
        public void ShouldReturnGoodTauxAssurance(bool estSportif, bool estFumeur, bool estCardiaques, bool estIngenieur,
            bool estPilote, double expected)
        {
            var result = Calculatrice.CalculTauxAssurance(estSportif, estFumeur, estCardiaques, estIngenieur, estPilote);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(100000, 3, 103000)]
        [InlineData(150000, 5, 157500)]
        [InlineData(250000, 10, 275000)]
        public void ShouldReturnGoodTotalAPayer(int emprunt, double taux, double expected)
        {
            var result = Calculatrice.CalculTotalAPayer(emprunt, taux);

            Assert.Equal(expected, result);
        }
    }


}