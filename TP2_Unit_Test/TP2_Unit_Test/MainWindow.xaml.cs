﻿using System.Windows;
using Model;

namespace TP2_Unit_Test
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var montant = int.Parse(this.montant.Text);
            var duree = int.Parse(this.duree.Text);
            var taux = double.Parse(this.taux.Text);

            var estSportif = this.sportBox.IsChecked;
            var estFumeur = this.fumetteBox.IsChecked;
            var estCardiaques = this.cardiaqueBox.IsChecked;
            var estIngenieur = this.ingenieurBox.IsChecked;
            var estPilote = this.piloteBox.IsChecked;

            var taux_assurance = Calculatrice.CalculTauxAssurance(estSportif != null && estSportif.Value, estFumeur != null && estFumeur.Value, estCardiaques != null && estCardiaques.Value, estIngenieur != null && estIngenieur.Value, estPilote != null && estPilote.Value);

            var mensualite = Calculatrice.CalculMensualite(montant, duree, taux);
            var mensualiteAssurance = montant * (taux_assurance/100) /12;

            var montantARembourser = Calculatrice.CalculTotalAPayer(montant, (taux));
            var montantARembourserAssurance = Calculatrice.CalculTotalAPayer(montant, taux_assurance);

            var TotalAPayer = montantARembourser + montantARembourserAssurance - montant;

            var aPayerParMois = mensualite + mensualiteAssurance;
            this.result.Content = $"Mensualité: {aPayerParMois}€";
            this.mensualiteAssurance.Content = $"Mensualité assurance: {mensualiteAssurance}€";
            this.totalInteret.Content = $"Total intérêt: {TotalAPayer - montant}€";
            this.totalAssurance.Content = $"Total assurance: {montantARembourserAssurance-montant}€";
            this.PayerApres10Ans.Content = $"Total rembourser après 10 ans : {aPayerParMois*120}";
        }
    }
}
